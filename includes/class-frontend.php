<?php
namespace Otomaties\Otomaties\Deployment;

class Frontend
{
	
	function __construct()
	{
		add_filter( 'admin_footer_text', array( $this, 'footer_text' ), 999 );
	}

	public function footer_text( $text ) {
		$version = ABSPATH . "version.txt";
		if( file_exists( $version ) ) {
			$myfile = fopen( $version, "r" );
			$text = fread( $myfile, filesize( $version ) );
			fclose($myfile);
		}

		$updates = ABSPATH . "updates.txt";
		if( file_exists( $updates ) ) {
			$text .= sprintf( ' | <span style="display: inline-block; background-color: #a00; width: 10px; height: 10px; border-radius: 10px;"> </span> <a href="%s?v=%s" target="_blank">%s</a>', home_url('/') . 'updates.txt', time(), __('Some updates have been performed since last deploy', 'otomaties-deployment-helper' ) );
		}
		else {
			$text .= sprintf( ' | <span style="display: inline-block; background-color: #46b450; width: 10px; height: 10px; border-radius: 10px;"> </span> ' . __('In sync with last deploy', 'otomaties-deployment-helper' ) );
		}
		return $text;
	}

}
new Frontend;