<?php
namespace Otomaties\Otomaties\Deployment;

class Plugin_Updater
{
	
	function __construct()
	{
		add_action( 'upgrader_process_complete', array( $this, 'write_updates_to_file' ), 10, 2 );
	}

	public function write_updates_to_file( $upgrader_object, $options ) {
		if ($options['action'] == 'update' && $options['type'] == 'plugin' ){
			foreach($options['plugins'] as $plugin){
				$errors = $upgrader_object->skin->get_errors();
				if( $errors->has_errors() ) {
					error_log(sprintf('There was an error updating plugin %s', $plugin ));
				}
				else{
					$this->write_to_file('plugin:' . $plugin);
				}

			}
		}
		if($options['action'] == 'update' && $options['type'] == 'theme' ) {
			foreach($options['themes'] as $theme){
				$this->write_to_file('theme:' . $theme);
			}
		}
		if($options['action'] == 'update' && $options['type'] == 'core' ) {
			$this->write_to_file('core');
		}
	}

	private function write_to_file($content) {
		$filename = ABSPATH . "updates.txt";
		if (!$handle = fopen($filename, 'a+')) {
			error_log( sprintf( "Cannot open file %s", $filename ) );
		}

		if (fwrite($handle, $content . PHP_EOL) === FALSE) {
			error_log( sprintf( "Cannot write to file %s", $filename ) );
		}
		fclose($handle);
	}

}
new Plugin_Updater;