<?php
namespace Otomaties\Otomaties\Deployment;

class Plugin_Installation
{
	
	function __construct()
	{
		add_action( 'admin_init', array( $this, 'redirect_installation_page' ), 999);
		add_action( 'admin_notices', array( $this, 'show_installation_notice' ), 1);
	}

	public function redirect_installation_page() {
		global $pagenow;
		$tab 	= filter_input(INPUT_GET, 'tab', FILTER_SANITIZE_STRING);
		$iframe = filter_input(INPUT_GET, 'TB_iframe', FILTER_VALIDATE_BOOLEAN);
		$plugin = filter_input(INPUT_GET, 'plugin', FILTER_SANITIZE_STRING);
		if ( $pagenow == 'plugin-install.php' ) {
			if( !$tab || !$iframe || !$plugin ) {
				wp_redirect( admin_url( 'plugins.php' ) . '?notice=disable-plugin-installation' );
				exit;
			}
		}
	}

	public function show_installation_notice() {
		$notice = filter_input(INPUT_GET, 'notice', FILTER_SANITIZE_STRING);
		if( $notice ):
			?>
			<div class="error notice">
				<p><?php printf( __( 'Plugin installation has been disabled. By Otomaties Deployment Helper. If this isn\'t a production environment, you could add %s to your wp-config.php', 'otomaties-deployment-helper' ), '<code>define( \'WP_ENV\', \'development\');</code>' ); ?></p>
			</div>
			<?php
		endif;
	}

}
new Plugin_Installation;