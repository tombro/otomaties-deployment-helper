=== Otomaties Deployment Helper ===
Contributors: tompoezie
Tags: deploy production plugin disable
Requires at least: 4.5
Tested up to: 5.4.1
Stable tag: 0.9.1

Disables plugin installation & keeps track of updated plugins.

== Description ==

This plugin will disable plugin installation on production environments. Set \`WP_ENV\` to anything else than production to enable plugin installation. The install will no longer be in sync with the development version, and any installed plugins will be removed when a new version is deployed.

== Changelog ==

= 0.9.1 =
* Move to bitbucket repo
* Added readme.txt
* Plugin-update-checker as a composer dependency
* Deleted WP Rocket's cache clearer, as this can be done with \`wp cli\`: \`wp rocket clean --confirm\` [GeekPress/wp-rocket-cli](https://github.com/GeekPress/wp-rocket-cli)