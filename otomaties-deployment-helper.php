<?php
/**
 * Plugin Name:     Otomaties Deployment Helper
 * Description:     Disables plugin installation & keeps track of updated plugins
 * Author:          Tom Broucke
 * Author URI:      https://tombroucke.be
 * Text Domain:     otomaties-deployment-helper
 * Domain Path:     /languages
 * Version:         0.9.1
 *
 * @package         Deployment_Helper
 */

namespace Otomaties\Deployment;

if ( ! defined( 'ABSPATH' ) ) exit;

class Deployment_Helper {

	private static $instance = null;

	/**
	 * Creates or returns an instance of this class.
	 * @since  1.0.0
	 * @return Plugin_Class_Name A single instance of this class.
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private function __construct() {
		$this->includes();
		$this->init();
	}

	private function includes() {

		require 'vendor/autoload.php';

		if ( !defined('WP_ENV') || WP_ENV == 'production') {
			include 'includes/class-frontend.php';
			include 'includes/class-plugin-installation.php';
			include 'includes/class-plugin-updater.php';
		}
	}

	private function init() {
		// Update checker
		\Puc_v4_Factory::buildUpdateChecker(
			'https://bitbucket.org/tombro/otomaties-deployment-helper',
			__FILE__,
			'otomaties-deployment-helper'
		);

		// Hooks
		add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );
	}

	public function load_plugin_textdomain() {
		load_muplugin_textdomain( 'otomaties-deployment-helper', basename( dirname( __FILE__ ) ) . '/languages' );
	}
}
Deployment_Helper::get_instance();