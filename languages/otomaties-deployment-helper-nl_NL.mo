��          �            x     y  =   �     �     �     �  �     2   �      �       "        1     >  =   T  E   �  -   �  ,       3  G   I     �     �     �  �   �  >   �  !   �     �  $   �     	     !	  5   7	  5   m	     �	           
                                      	                              Check for updates Disables plugin installation & keeps track of updated plugins In sync with last deploy More information about %s Otomaties Deployment Helper Plugin installation has been disabled. By Otomaties Deployment Helper. If this isn't a production environment, you could add %s to your wp-config.php Some updates have been performed since last deploy There is no changelog available. Tom Broucke Unknown update checker status "%s" View details https://tombroucke.be the plugin titleA new version of the %s plugin is available. the plugin titleCould not determine if updates are available for %s. the plugin titleThe %s plugin is up to date. Project-Id-Version: Otomaties Rsync Deployment Helper
POT-Creation-Date: 2020-04-24 17:33+0200
PO-Revision-Date: 2020-04-24 17:34+0200
Last-Translator: Tom Broucke <tom@tombrouckeBe>
Language-Team: Tom Broucke <tom@tombrouckeBe>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: otomaties-rsync-deployment-helper.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Controleer op updates Installatie van plug-ins uitschakelen & bijhouden van geüpdate plugins In sync met laatste deploy Meer informatie over %s Otomaties Deployment Helper De installatie van de plug-in is uitgeschakeld door Otomaties Deployment Helper. Als dit geen productieomgeving is, u %s toevoegen aan uw wp-config.php bestand Sommige updates zijn uitgevoerd sinds de laatste implementatie Er is geen changelog beschikbaar. Tom Broucke Onbekende update checker status "%s" Bekijk details https://tombroucke.be Er is een nieuwe versie van de %s plugin beschikbaar. Kon niet bepalen of updates beschikbaar zijn voor %s. De %s plugin is bijgewerkt. 